package com.hcknl.mesaitakip.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.hcknl.mesaitakip.R;
import com.hcknl.mesaitakip.adapters.LogsAdapter;
import com.hcknl.mesaitakip.core.PopupUtils;
import com.hcknl.mesaitakip.db.DatabaseSession;
import com.hcknl.mesaitakip.db.LogEntity;

import java.util.List;

public class LogsActivity extends AppCompatActivity implements LogsAdapter.IRowClickListener {

    private LogsAdapter dataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayShowTitleEnabled(false);
        bindRecyclerView();

    }

    private void bindRecyclerView() {
        List<LogEntity> logs = DatabaseSession.getInstance(this).getLogs();
        if (logs == null) return;
        if (logs.size() == 0) return;

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.log_list);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        dataAdapter = new LogsAdapter(logs, this);
        dataAdapter.setRowClickListener(this);
        recyclerView.setAdapter(dataAdapter);

    }

    @Override
    public void onRowClick(int position) {
        if (dataAdapter == null) return;
        final int id = dataAdapter.getLogItemId(position);
        if (id == -1) return;
        PopupUtils.createPopup(this, R.string.delete_record_title, R.string.delete_record_title_explanation, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                DatabaseSession.getInstance(LogsActivity.this).deleteRecord(id);
                dataAdapter.removeItem(id);
            }
        });
    }
}
