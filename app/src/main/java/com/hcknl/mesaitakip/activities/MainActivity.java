package com.hcknl.mesaitakip.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.hcknl.mesaitakip.R;
import com.hcknl.mesaitakip.alarms.AlarmHandler;
import com.hcknl.mesaitakip.core.PopupUtils;
import com.hcknl.mesaitakip.core.TimeUtils;
import com.hcknl.mesaitakip.db.DatabaseSession;
import com.hcknl.mesaitakip.db.LogEntity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView entranceValueView;
    private TextView exitValueView;
    private TextView timeLeftValueView;

    private CountDownTimer timer;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()) {
            case R.id.action_logs:
                intent.setClass(this, LogsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_settings:
                intent.setClass(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayShowTitleEnabled(false);


        Button setEntranceBtn = (Button) findViewById(R.id.set_entrance_btn);
        setEntranceBtn.setOnClickListener(this);
        Button setExitBtn = (Button) findViewById(R.id.set_exit_btn);
        setExitBtn.setOnClickListener(this);

        entranceValueView = (TextView) findViewById(R.id.entrance_value);
        exitValueView = (TextView) findViewById(R.id.exit_value);
        timeLeftValueView = (TextView) findViewById(R.id.time_left_value);

        doEntranceIfAlreadyStarted();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.set_entrance_btn:
                PopupUtils.createPopup(this, R.string.entrance_title, R.string.entrance_content, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        doEntrance();
                    }
                });
                break;
            case R.id.set_exit_btn:
                PopupUtils.createPopup(this, R.string.exit_title, 0, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        doExit(true);
                    }
                });
                break;
        }
    }

    private void doEntranceIfAlreadyStarted() {
        LogEntity logEntity = DatabaseSession.getInstance(this).getLastInsertedRecord();
        if (logEntity == null) return;
        long entranceTime = logEntity.getEntranceTime();
        long estimatedExitTime = logEntity.getEstimatedExitTime();
        if (logEntity.isExited() || (entranceTime <= 0 || estimatedExitTime <= 0)) {
            return;
        }

        entranceValueView.setText(TimeUtils.getEntranceTimeString(entranceTime));
        exitValueView.setText(TimeUtils.timeToString(estimatedExitTime));

        long currentTimeMillis = TimeUtils.getCurrentTimeMillis();
        createCountDownTimer(currentTimeMillis, estimatedExitTime);
    }

    private void doEntrance() {

        dismissCountDownTimer();

        long entranceTime = TimeUtils.getCurrentTimeMillis();
        long exitTime = TimeUtils.getExitTime(entranceTime);

        String entranceString = TimeUtils.getEntranceTimeString(entranceTime);
        String exitString = TimeUtils.getExitTimeString(entranceTime);

        DatabaseSession.getInstance(this).doEntrance(entranceTime);

        entranceValueView.setText(entranceString);
        exitValueView.setText(exitString);

        createCountDownTimer(entranceTime, exitTime);

        int hour = TimeUtils.getHour(exitString);
        int minute = TimeUtils.getMinute(exitString);

        AlarmHandler.cancelAlarm(this, AlarmHandler.ACTION_BROADCAST_EXIT, AlarmHandler.REQUEST_BROADCAST_EXIT);
        AlarmHandler.setAlarm(this, hour, minute, AlarmHandler.ACTION_BROADCAST_EXIT, AlarmHandler.REQUEST_BROADCAST_EXIT);

    }

    private void doExit(boolean manuel) {

        if (manuel) {
            DatabaseSession.getInstance(this).doExit(TimeUtils.getCurrentTimeMillis());
            entranceValueView.setText(getString(R.string.default_time));
            exitValueView.setText(getString(R.string.default_time));
            timeLeftValueView.setText(getString(R.string.default_time));
            AlarmHandler.cancelAlarm(this, AlarmHandler.ACTION_BROADCAST_EXIT, AlarmHandler.REQUEST_BROADCAST_EXIT);
        }

        dismissCountDownTimer();

    }

    private void createCountDownTimer(final long entranceTime, long exitTime) {
        if (timer != null) {
            return;
        }
        timer = new CountDownTimer(exitTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String time = TimeUtils.calculateDuration(entranceTime, millisUntilFinished);
                if (time == null) {
                    onFinish();
                    return;
                }
                timeLeftValueView.setText(time);
            }

            @Override
            public void onFinish() {
                doExit(false);
            }
        };
        timer.start();
    }

    private void dismissCountDownTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }
}
