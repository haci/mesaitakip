package com.hcknl.mesaitakip.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcknl.mesaitakip.R;
import com.hcknl.mesaitakip.core.ColorResource;
import com.hcknl.mesaitakip.core.TimeUtils;
import com.hcknl.mesaitakip.db.LogEntity;

import java.util.List;


public class LogsAdapter extends RecyclerView.Adapter<LogsAdapter.ContentViewHolder> {

    private final List<LogEntity> logs;
    private final String exitStatusDone;
    private final String exitStatusNo;
    private final Context context;

    private IRowClickListener rowClickListener;

    public interface IRowClickListener {
        void onRowClick(int position);
    }

    public void setRowClickListener(IRowClickListener rowClickListener) {
        this.rowClickListener = rowClickListener;
    }

    public LogsAdapter(List<LogEntity> logs, Context context) {
        this.logs = logs;
        exitStatusDone = context.getResources().getString(R.string.exit_status_ok);
        exitStatusNo = context.getResources().getString(R.string.exit_status_no);
        this.context = context;
    }

    @Override
    public LogsAdapter.ContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_log_data_adapter, parent, false);
        return new ContentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LogsAdapter.ContentViewHolder holder, int position) {
        if (logs == null) return;
        LogEntity entity = logs.get(position);
        if (entity == null) return;

        holder.headerView.setText(TimeUtils.getEntranceDayString(entity.getEntranceTime()));

        holder.entranceValueView.setText(TimeUtils.getEntranceTimeString(entity.getEntranceTime()));

        if (entity.getExitTime() > 0)
            holder.exitValueView.setText(TimeUtils.timeToString(entity.getExitTime()));
        if (entity.getEstimatedExitTime() > 0)
            holder.estimatedExitView.setText(TimeUtils.timeToString(entity.getEstimatedExitTime()));

        holder.totalTime.setText(TimeUtils.getWorkDuration(entity.getEntranceTime(), entity.getExitTime()));

        boolean isExited = entity.isExited();
        holder.exitStatusView.setText(isExited ? exitStatusDone : exitStatusNo);
        if (isExited) {
            holder.exitStatusView.setTextColor(ColorResource.getInstance(context).getMaterialGreen());
        } else {
            holder.exitStatusView.setTextColor(ColorResource.getInstance(context).getMaterialRed());
        }

    }


    @Override
    public int getItemCount() {
        int size = adapterDataCheck();
        if (size == -1) size = 0;
        return size;
    }

    public int getLogItemId(int position) {
        int size = adapterDataCheck();
        int id = -1;
        if (size == -1) return id;
        if (position > logs.size()) return id;
        return logs.get(position).getId();
    }

    /**
     * must be called on UI thread
     */
    public void removeItem(int id) {
        int size = adapterDataCheck();
        if (size == -1) return;

        int entityIndex = -1;
        for (int i = 0; i < logs.size(); i++) {
            LogEntity entity = logs.get(i);
            if (entity.getId() == id) {
                entityIndex = i;
                break;
            }
        }
        if (entityIndex >= 0) {
            logs.remove(entityIndex);
            notifyItemRemoved(entityIndex);
        }

    }

    /**
     * returns -1 if an unusual situtation occurred
     */
    private int adapterDataCheck() {
        int size = -1;
        if (logs == null) return size;
        if (logs.size() == 0) return size;
        return logs.size();
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {

        private final TextView entranceValueView;
        private final TextView exitValueView;
        private final TextView totalTime;
        private final TextView headerView;
        private final TextView exitStatusView;
        private final TextView estimatedExitView;
        private final ImageView icDeleteView;

        ContentViewHolder(View itemView) {
            super(itemView);
            headerView = (TextView) itemView.findViewById(R.id.date_header);
            entranceValueView = (TextView) itemView.findViewById(R.id.entrance_value);
            exitValueView = (TextView) itemView.findViewById(R.id.exit_value);
            totalTime = (TextView) itemView.findViewById(R.id.total_time_value);
            exitStatusView = (TextView) itemView.findViewById(R.id.exit_status_value);
            estimatedExitView = (TextView) itemView.findViewById(R.id.estimated_exit_value);
            icDeleteView = (ImageView) itemView.findViewById(R.id.ic_delete_view);

            if (rowClickListener != null) {
                icDeleteView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rowClickListener.onRowClick(getAdapterPosition());
                    }
                });
            }
        }
    }

}


