package com.hcknl.mesaitakip.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;

import com.hcknl.mesaitakip.R;
import com.hcknl.mesaitakip.core.TimeUtils;

public class TimePreference extends DialogPreference {
    private int mHour = 0;
    private int mMinute = 0;
    private TimePicker picker = null;
    public static final String DEFAULT_VALUE = "10:00";

    public TimePreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TimePickerAttrs);

        String prefsTitle = typedArray.getString(R.styleable.TimePickerAttrs_time_picker_title);
        int hour = typedArray.getInt(R.styleable.TimePickerAttrs_time_picker_default_hour, 0);
        int minute = typedArray.getInt(R.styleable.TimePickerAttrs_time_picker_default_minute, 0);
        String prefsDialogPositiveText = typedArray.getString(R.styleable.TimePickerAttrs_time_picker_positive_text);
        String prefsDialogNegativeText = typedArray.getString(R.styleable.TimePickerAttrs_time_picker_negative_text);

        typedArray.recycle();

        setTitle(prefsTitle);
        setPositiveButtonText(prefsDialogPositiveText);
        setNegativeButtonText(prefsDialogNegativeText);

        setTime(hour, minute);
        updateSummary();
    }

    private void setTime(int hour, int minute) {
        mHour = hour;
        mMinute = minute;
        String time = TimeUtils.toTime(mHour, mMinute);
        persistString(time);
        notifyDependencyChange(shouldDisableDependents());
        notifyChanged();
    }


    public void updateSummary() {
        String min = "";
        if (mMinute < 10) {
            min = "0";
        }
        min += String.valueOf(mMinute);
        String time = String.valueOf(mHour) + ":" + min;
        setSummary(time);
    }

    @Override
    protected View onCreateDialogView() {
        picker = new TimePicker(getContext());
        picker.setIs24HourView(true);
        return picker;
    }

    @Override
    protected void onBindDialogView(View v) {
        super.onBindDialogView(v);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            picker.setHour(mHour);
            picker.setMinute(mMinute);
        } else {
            picker.setCurrentHour(mHour);
            picker.setCurrentMinute(mMinute);
        }
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            int currHour;
            int currMinute;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                currHour = picker.getHour();
                currMinute = picker.getMinute();
            } else {
                currHour = picker.getCurrentHour();
                currMinute = picker.getCurrentMinute();
            }

            if (!callChangeListener(TimeUtils.toTime(currHour, currMinute))) {
                return;
            }

            setTime(currHour, currMinute);
            updateSummary();
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        String time;

        if (restorePersistedValue) {
            if (defaultValue == null) {
                time = getPersistedString(DEFAULT_VALUE);
            } else {
                time = getPersistedString(DEFAULT_VALUE);
            }
        } else {
            time = defaultValue.toString();
        }

        int currHour = TimeUtils.getHour(time);
        int currMinute = TimeUtils.getMinute(time);
        setTime(currHour, currMinute);
        updateSummary();
    }
}