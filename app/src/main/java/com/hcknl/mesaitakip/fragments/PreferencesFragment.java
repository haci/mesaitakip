package com.hcknl.mesaitakip.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.MenuItem;

import com.hcknl.mesaitakip.R;
import com.hcknl.mesaitakip.activities.SettingsActivity;
import com.hcknl.mesaitakip.alarms.AlarmHandler;
import com.hcknl.mesaitakip.core.PreferencesUtils;
import com.hcknl.mesaitakip.core.TimeUtils;
import com.hcknl.mesaitakip.views.TimePreference;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PreferencesFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_general);
        setHasOptionsMenu(true);
        context = getActivity();

        bindPreferenceSummaryToValue(findPreference(PreferencesUtils.KEY_DISPLAY_NAME));
        bindPreferenceSummaryToValue(findPreference(PreferencesUtils.KEY_NOTIFICATION_RINGTONE));
        bindPreferenceSummaryToValue(findPreference(PreferencesUtils.KEY_ALARM_TIME_PICKER));
        bindPreferenceSummaryToValue(findPreference(PreferencesUtils.KEY_NOTIFICATION_STATE));
        bindPreferenceSummaryToValue(findPreference(PreferencesUtils.KEY_DAILY_WORK_TIME));
    }

    private void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(this);
        if (preference.getKey().equals(PreferencesUtils.KEY_NOTIFICATION_STATE)) {
            onPreferenceChange(preference, PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getBoolean(preference.getKey(), false));
        } else if (preference.getKey().equals(PreferencesUtils.KEY_ALARM_TIME_PICKER)) {
            onPreferenceChange(preference, PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(), TimePreference.DEFAULT_VALUE));
        } else if (preference.getKey().equals(PreferencesUtils.KEY_DAILY_WORK_TIME)) {
            onPreferenceChange(preference, PreferencesUtils.getDailyWorkTime());
        } else {
            onPreferenceChange(preference, PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(), ""));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            startActivity(new Intent(getActivity(), SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference.getKey().equals(PreferencesUtils.KEY_NOTIFICATION_RINGTONE)) {
            String stringValue = newValue.toString();
            if (TextUtils.isEmpty(stringValue)) {
                preference.setSummary(R.string.pref_ringtone_silent);
            } else {
                Ringtone ringtone = RingtoneManager.getRingtone(preference.getContext(), Uri.parse(stringValue));
                if (ringtone == null) {
                    preference.setSummary(null);
                } else {
                    String name = ringtone.getTitle(preference.getContext());
                    preference.setSummary(name);
                }
            }

        } else if (preference.getKey().equals(PreferencesUtils.KEY_ALARM_TIME_PICKER)) {
            String stringValue = newValue.toString();
            TimePreference timePreference = (TimePreference) preference;
            timePreference.updateSummary();
            int hour = TimeUtils.getHour(stringValue);
            int minute = TimeUtils.getMinute(stringValue);
            AlarmHandler.setAlarm(context, hour, minute, AlarmHandler.ACTION_BROADCAST, AlarmHandler.REQUEST_BROADCAST);
        } else if (preference.getKey().equals(PreferencesUtils.KEY_NOTIFICATION_STATE)) {
            boolean enabled = (boolean) newValue;
            if (!enabled) {
                AlarmHandler.cancelAlarm(context, AlarmHandler.ACTION_BROADCAST, AlarmHandler.REQUEST_BROADCAST);
                AlarmHandler.cancelAlarm(context, AlarmHandler.ACTION_BROADCAST_EXIT, AlarmHandler.REQUEST_BROADCAST_EXIT);
            }
        } else if (preference.getKey().equals(PreferencesUtils.KEY_DAILY_WORK_TIME)) {
            String stringValue = newValue.toString();
            int hour = TimeUtils.getHour(stringValue);
            int minute = TimeUtils.getMinute(stringValue);
            String min = "";
            if (minute < 10) {
                min = "0";
            }
            min += String.valueOf(minute);
            String time = String.valueOf(hour) + ":" + min;
            preference.setSummary(time);

        } else {
            String stringValue = newValue.toString();
            preference.setSummary(stringValue);
        }
        return true;
    }
}