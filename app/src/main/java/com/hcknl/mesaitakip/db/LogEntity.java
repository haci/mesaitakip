package com.hcknl.mesaitakip.db;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Log")
public class LogEntity {
    private static final String _ID = "_id";
    static final String ENTRANCE_TIME = "entrance_time";
    static final String EXIT_TIME = "exit_time";
    private static final String ESTIMATED_EXIT_TIME = "estimated_exit_time";
    static final String IS_EXITED = "is_exited";

    @DatabaseField(columnName = _ID, generatedId = true)
    private int _id;
    @DatabaseField(columnName = ENTRANCE_TIME)
    private long entranceTime;
    @DatabaseField(columnName = EXIT_TIME)
    private long exitTime;
    @DatabaseField(columnName = ESTIMATED_EXIT_TIME)
    private long estimatedExitTime;
    @DatabaseField(columnName = IS_EXITED)
    private boolean isExited;

    public int getId() {
        return _id;
    }

    public long getEntranceTime() {
        return entranceTime;
    }

    void setEntranceTime(long entranceTime) {
        this.entranceTime = entranceTime;
    }

    public long getEstimatedExitTime() {
        return estimatedExitTime;
    }

    void setEstimatedExitTime(long estimatedExitTime) {
        this.estimatedExitTime = estimatedExitTime;
    }

    public long getExitTime() {
        return exitTime;
    }

    public boolean isExited() {
        return isExited;
    }

    @Override
    public String toString() {
        return "LogEntity{" +
                "_id=" + _id +
                ", entranceTime=" + entranceTime +
                ", estimatedExitTime=" + estimatedExitTime +
                ", exitTime=" + exitTime +
                ", isExited=" + isExited +
                '}';
    }
}
