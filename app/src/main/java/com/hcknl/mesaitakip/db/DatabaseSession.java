package com.hcknl.mesaitakip.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.hcknl.mesaitakip.R;
import com.hcknl.mesaitakip.core.TimeUtils;

import java.sql.SQLException;
import java.util.List;

public class DatabaseSession extends OrmLiteSqliteOpenHelper {

    private final String LOG_TAG = getClass().getSimpleName();

    private static final String DATABASE_NAME = "data.db";

    private static final int DATABASE_VERSION = 1;

    private static DatabaseSession helper;

    public synchronized static DatabaseSession getInstance(Context context) {
        if (helper == null) {
            helper = new DatabaseSession(context);
        }
        return helper;
    }

    private DatabaseSession(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    private RuntimeExceptionDao<LogEntity, Integer> getLogDao() {
        return getRuntimeExceptionDao(LogEntity.class);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i(LOG_TAG, "onCreate");
            TableUtils.createTable(connectionSource, LogEntity.class);
        } catch (SQLException e) {
            Log.e(LOG_TAG, "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
    }

    public List<LogEntity> getLogs() {
        RuntimeExceptionDao<LogEntity, Integer> simpleDao = getLogDao();
        return simpleDao.queryForAll();
    }

    private void addNewLog(LogEntity logEntity) {
        getLogDao().create(logEntity);
    }

    public void doEntrance(long entranceDate) {
        LogEntity entity = new LogEntity();
        entity.setEntranceTime(entranceDate);
        entity.setEstimatedExitTime(TimeUtils.getExitTime(entranceDate));
        addNewLog(entity);
    }

    public void doExit(long currentTimeMillis) {
        LogEntity lastEntity = getLastInsertedRecord();
        if (lastEntity == null) return;

        UpdateBuilder updateBuilder = getLogDao().updateBuilder();
        try {
            updateBuilder.where().eq(LogEntity.ENTRANCE_TIME, lastEntity.getEntranceTime());
            updateBuilder.updateColumnValue(LogEntity.IS_EXITED, true);
            updateBuilder.updateColumnValue(LogEntity.EXIT_TIME, currentTimeMillis);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public LogEntity getLastInsertedRecord() {
        List<LogEntity> entities = getLogDao().queryForAll();
        if (entities.size() == 0) return null;
        return entities.get(entities.size() - 1);
    }

    public void deleteRecord(int id) {
        getLogDao().deleteById(id);
    }

}