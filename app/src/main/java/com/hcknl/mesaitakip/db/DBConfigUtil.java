package com.hcknl.mesaitakip.db;


import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

class DBConfigUtil extends OrmLiteConfigUtil {
    private static final Class<?>[] classes = new Class[]{
            LogEntity.class
    };

    public static void main(String[] args) throws Exception {
        writeConfigFile("ormlite_config.txt", classes);
    }
}