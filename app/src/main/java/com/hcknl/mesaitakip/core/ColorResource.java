package com.hcknl.mesaitakip.core;


import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;

import com.hcknl.mesaitakip.R;

public class ColorResource {

    private static ColorResource holder = null;

    @ColorInt
    private final int materialRed;
    @ColorInt
    private final int materialGreen;


    public static ColorResource getInstance(Context context) {
        if (holder == null) {
            holder = new ColorResource(context);
        }

        return holder;
    }

    private ColorResource(Context context) {
        materialRed = ContextCompat.getColor(context, R.color.material_red);
        materialGreen = ContextCompat.getColor(context, R.color.colorAccent);
    }

    public int getMaterialGreen() {
        return materialGreen;
    }

    public int getMaterialRed() {
        return materialRed;
    }
}
