package com.hcknl.mesaitakip.core;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferencesUtils {

    private static SharedPreferences sharedPref;

    public static final String KEY_DISPLAY_NAME = "name";
    public static final String KEY_NOTIFICATION_STATE = "notifications_new_message";
    public static final String KEY_NOTIFICATION_RINGTONE = "notifications_new_message_ringtone";
    public static final String KEY_ALARM_TIME_PICKER = "time_picker";
    public static final String KEY_DAILY_WORK_TIME = "work_time";

    static void init(Context context) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static String getDisplayName() {
        return sharedPref.getString(KEY_DISPLAY_NAME, "");
    }

    public static String getRingtoneUri() {
        return sharedPref.getString(KEY_NOTIFICATION_RINGTONE, "");
    }

    public static String getDailyWorkTime() {
        return sharedPref.getString(KEY_DAILY_WORK_TIME, "9:00");
    }
}
