package com.hcknl.mesaitakip.core;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeUtils {

    public static final String HH_MM_SS = "HH:mm:ss";

    /**
     * Used In LogAdapter header
     * Millis converter to date
     *
     * @param timeInMillis entranceTimeInMillis
     * @return String only contains dd:mm:yyyy
     */
    public static String getEntranceDayString(long timeInMillis) {
        return getDateInstance(millisToDate(timeInMillis));
    }

    public static String getEntranceTimeString(long timeInMillis) {
        return dateToString(millisToDate(timeInMillis));
    }

    public static String getExitTimeString(long entranceTime) {
        String value = PreferencesUtils.getDailyWorkTime();
        int hour = getHour(value);
        int minute = getMinute(value);
        int totalMinutes = (hour * 60) + minute;
        return dateToString(millisToDate(entranceTime, totalMinutes));
    }

    public static long getExitTime(long entranceTime) {

        String value = PreferencesUtils.getDailyWorkTime();
        int hour = getHour(value);
        int minute = getMinute(value);
        int totalMinutes = (hour * 60) + minute;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(entranceTime);
        calendar.add(Calendar.MINUTE, totalMinutes);
        Date time = calendar.getTime();
        return time.getTime();
    }

    public static String calculateDuration(long entrance, long millisUntilFinished) {
        long diff = millisUntilFinished - entrance;

        if (diff <= 0) {
            return null;
        }

        int seconds = (int) diff / 1000;
        int minutes = (seconds / 60);
        seconds %= 60;
        int hours = minutes / 60;
        minutes %= 60;

        String strSeconds = String.valueOf(seconds);
        String strMins = String.valueOf(minutes);
        String strHours = String.valueOf(hours);
        if (seconds < 10) {
            strSeconds = "0" + seconds;
        }
        if (minutes < 10) {
            strMins = "0" + minutes;
        }
        if (hours < 10) {
            strHours = "0" + hours;
        }

        return strHours + ":" + strMins + ":" + strSeconds;
    }

    private static Date millisToDate(long millis, int dailyWorkMins) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);
        if (dailyWorkMins > 0) {
            cal.add(Calendar.MINUTE, dailyWorkMins);
        }
        return cal.getTime();
    }

    public static Date millisToDate(long millis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);
        return cal.getTime();
    }

    public static String timeToString(long entranceTime) {
        return dateToString(millisToDate(entranceTime));
    }

    public static long getCurrentTimeMillis() {
        return Calendar.getInstance(Locale.getDefault()).getTimeInMillis();
    }

    public static String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat(TimeUtils.HH_MM_SS, Locale.getDefault());
        return df.format(date);
    }

    private static String getDateInstance(Date date) {
        DateFormat df = DateFormat.getDateInstance();
        return df.format(date);
    }

    public static String getWorkDuration(long entranceDate, long exitDate) {
        return calculateDuration(entranceDate, exitDate);
    }

    public static int getHour(String time) {
        try {
            if (time == null || time.length() == 0) return 0;
            String[] pieces = time.split(":");
            if (pieces.length == 0) return 0;
            String firstPiece = pieces[0];
            firstPiece = firstPiece.replaceAll("[^0-9]+", "");
            if (firstPiece.equals("")) return 0;
            return Integer.parseInt(firstPiece);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int getMinute(String time) {
        try {
            if (time == null || time.length() == 0 || time.length() == 1) return 0;
            String[] pieces = time.split(":");
            if (pieces.length == 1 || pieces.length == 0) return 0;
            String secondPiece = pieces[1];
            secondPiece = secondPiece.replaceAll("[^0-9]+", "");
            if (secondPiece.equals("")) return 0;
            return Integer.parseInt(secondPiece);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String toTime(int hour, int minute) {
        return String.valueOf(hour) + ":" + String.valueOf(minute);
    }
}
