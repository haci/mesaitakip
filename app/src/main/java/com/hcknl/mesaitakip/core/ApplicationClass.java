package com.hcknl.mesaitakip.core;

import android.app.Application;

import com.hcknl.mesaitakip.db.DatabaseSession;


public class ApplicationClass extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        DatabaseSession.getInstance(this);
        PreferencesUtils.init(this);

    }
}
