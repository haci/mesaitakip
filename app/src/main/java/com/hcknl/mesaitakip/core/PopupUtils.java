package com.hcknl.mesaitakip.core;

import android.content.Context;
import android.support.annotation.StringRes;

import com.afollestad.materialdialogs.MaterialDialog;
import com.hcknl.mesaitakip.R;

public class PopupUtils {

    public static void createPopup(Context context, @StringRes int title, @StringRes int content, MaterialDialog.SingleButtonCallback callback) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(context);
        if (content > 0) {
            builder.content(content);
        }
        if (title > 0) {
            builder.title(title);
        }

        builder.positiveText(R.string.yes)
                .negativeText(R.string.no)
                .negativeColorRes(R.color.colorPrimaryDark)
                .positiveColorRes(R.color.colorPrimaryDark)
                .onPositive(callback)
                .show();
    }
}