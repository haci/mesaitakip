package com.hcknl.mesaitakip.alarms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationHandler notifications = new NotificationHandler(context);

        String action = intent.getAction();
        if (!TextUtils.isEmpty(action) && action.equals(AlarmHandler.ACTION_BROADCAST)) {
            notifications.sendNotification(notifications.createEnterNotification());
        } else {
            notifications.sendNotification(notifications.createExitNotification());
        }
    }
}


