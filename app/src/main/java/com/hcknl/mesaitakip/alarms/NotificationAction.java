package com.hcknl.mesaitakip.alarms;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import com.hcknl.mesaitakip.core.TimeUtils;
import com.hcknl.mesaitakip.db.DatabaseSession;

public class NotificationAction extends IntentService {
    private final static String TAG = NotificationAction.class.getSimpleName();

    public NotificationAction() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        NotificationHandler.NotificationParams.Type type = (NotificationHandler.NotificationParams.Type) intent.getSerializableExtra("type");
        if (type == null) return;

        if (type == NotificationHandler.NotificationParams.Type.ENTER) {

            long entranceTime = TimeUtils.getCurrentTimeMillis();
            String exitString = TimeUtils.getExitTimeString(entranceTime);

            DatabaseSession.getInstance(this).doEntrance(entranceTime);

            int hour = TimeUtils.getHour(exitString);
            int minute = TimeUtils.getMinute(exitString);

            AlarmHandler.cancelAlarm(this, AlarmHandler.ACTION_BROADCAST_EXIT, AlarmHandler.REQUEST_BROADCAST_EXIT);
            AlarmHandler.setAlarm(this, hour, minute, AlarmHandler.ACTION_BROADCAST_EXIT, AlarmHandler.REQUEST_BROADCAST_EXIT);
        } else {
            DatabaseSession.getInstance(this).doExit(TimeUtils.getCurrentTimeMillis());
            AlarmHandler.cancelAlarm(this, AlarmHandler.ACTION_BROADCAST_EXIT, AlarmHandler.REQUEST_BROADCAST_EXIT);
        }


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

    }
}
