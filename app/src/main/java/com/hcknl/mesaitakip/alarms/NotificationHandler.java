package com.hcknl.mesaitakip.alarms;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.hcknl.mesaitakip.R;
import com.hcknl.mesaitakip.activities.MainActivity;
import com.hcknl.mesaitakip.core.PreferencesUtils;
import com.hcknl.mesaitakip.core.TimeUtils;

import java.io.Serializable;

class NotificationHandler {

    private final Context context;

    NotificationHandler(Context context) {
        this.context = context;
    }

    NotificationCompat.Builder createExitNotification() {
        return getBuilder(NotificationParams.Type.EXIT);
    }

    NotificationCompat.Builder createEnterNotification() {
        return getBuilder(NotificationParams.Type.ENTER);
    }

    @NonNull
    private NotificationCompat.Builder getBuilder(NotificationParams.Type notificationType) {

        NotificationParams notificationParams = new NotificationParams(notificationType, context).fill();

        return createNotification(notificationParams);
    }

    @NonNull
    private NotificationCompat.Builder createNotification(NotificationParams notificationParams) {

        Intent notificationIntent = new Intent(context, MainActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);


        NotificationCompat.Builder b = new NotificationCompat.Builder(context);
        b.setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.colorAccent))
                .setSmallIcon(notificationParams.getNotificationIcon())
                .setContentTitle(notificationParams.getContentTitle())
                .setContentText(notificationParams.getContentText())
                .setSubText(notificationParams.getTime())
                .setAutoCancel(true)
                .setContentIntent(intent)
                .setPriority(NotificationCompat.PRIORITY_MAX);

        if (notificationParams.getRingtoneUri() != null) {
            b.setSound(notificationParams.getRingtoneUri());
        }

        return b;
    }

    void sendNotification(NotificationCompat.Builder builder) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) System.currentTimeMillis(), builder.build());
    }

    static class NotificationParams {

        enum Type implements Serializable {
            EXIT,
            ENTER
        }

        private String contentText;
        private Uri ringtoneUri;
        private String contentTitle;
        private String time;
        private final Type notificationType;
        private final Context context;
        private int notificationIcon;
        private String actionButtonTitle;

        NotificationParams(Type notificationType, Context context) {
            this.notificationType = notificationType;
            this.context = context;
        }

        Type getNotificationType() {
            return notificationType;
        }

        String getActionButtonTitle() {
            return actionButtonTitle;
        }

        int getNotificationIcon() {
            return notificationIcon;
        }

        String getContentText() {
            return contentText;
        }

        Uri getRingtoneUri() {
            return ringtoneUri;
        }

        String getContentTitle() {
            return contentTitle;
        }

        public String getTime() {
            return time;
        }

        NotificationParams fill() {
            String displayName = PreferencesUtils.getDisplayName();
            if (displayName.length() == 0) {
                if (notificationType == Type.ENTER) {
                    contentText = context.getString(R.string.notification_content_enter_no_name);
                } else {
                    contentText = context.getString(R.string.notification_content_exit_no_name);
                }
            } else {
                if (notificationType == Type.ENTER) {
                    contentText = context.getString(R.string.notification_content_enter, displayName);
                } else {
                    contentText = context.getString(R.string.notification_content_exit, displayName);
                }
            }

            if (notificationType == Type.ENTER) {
                notificationIcon = R.drawable.ic_notification;
                actionButtonTitle = context.getString(R.string.action_text_enter);
            } else {
                notificationIcon = R.drawable.ic_notification_exit;
                actionButtonTitle = context.getString(R.string.action_text_exit);
            }

            String uriString = PreferencesUtils.getRingtoneUri();
            ringtoneUri = null;
            if (uriString.length() > 0) {
                ringtoneUri = Uri.parse(uriString);
            }
            contentTitle = context.getString(R.string.notification_content_title);

            time = TimeUtils.dateToString(TimeUtils.millisToDate(System.currentTimeMillis()));
            return this;
        }
    }
}
